import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(JUnit4.class)
public class VerticalXORTest {

	@Test
	public void shouldMatchOnlyEvenRows() {
		final int COL = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorVertically(BitMatrix.EVEN_TRUE, COL);

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, COL));
		}

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, COL));
		}
	}

	@Test
	public void shouldMatchOnlyOddRows() {
		final int COL = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorVertically(BitMatrix.ODD_TRUE, COL);

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, COL));
		}

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, COL));
		}

	}

	@Test
	public void shouldMatchWholeColumn() {
		final int COL = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorVertically(BitMatrix.ALL_TRUE, COL);

		for(int row = 0; row < matrix.getHeightInBits(); row++) {
			assertTrue(matrix.get(row, COL));
		}
	}

	@Test
	public void shouldBeWholeColumnFalse() {
		final int COL = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorVertically(BitMatrix.ALL_TRUE, COL);
		matrix.xorVertically(BitMatrix.ALL_TRUE, COL);

		for(int row = 0; row < matrix.getHeightInBits(); row++) {
			assertFalse(matrix.get(row, COL));
		}
	}
}
