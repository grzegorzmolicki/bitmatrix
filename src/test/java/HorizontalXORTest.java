import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(JUnit4.class)
public class HorizontalXORTest {
	@Test
	public void shouldMatchOnlyEvenColumns() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorHorizontally(BitMatrix.EVEN_TRUE, ROW);

		for(int col = 0; col < matrix.getWidthInBits(); col += 2) {
			assertTrue(matrix.get(ROW, col));
		}

		for(int col = 1; col < matrix.getWidthInBits(); col += 2) {
			assertFalse(matrix.get(ROW, col));
		}
	}

	@Test
	public void shouldMatchOnlyOddRows() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorHorizontally(BitMatrix.ODD_TRUE, ROW);

		for(int col = 1; col < matrix.getWidthInBits(); col += 2) {
			assertTrue(matrix.get(ROW, col));
		}

		for(int col = 0; col < matrix.getWidthInBits(); col += 2) {
			assertFalse(matrix.get(ROW, col));
		}

	}

	@Test
	public void shouldMatchWholeColumn() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorHorizontally(BitMatrix.ALL_TRUE, ROW);

		for(int col = 0; col < matrix.getWidthInBits(); col++) {
			assertTrue(matrix.get(ROW, col));
		}
	}

	@Test
	public void shouldBeWholeColumnFalse() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();

		matrix.xorHorizontally(BitMatrix.ALL_TRUE, ROW);
		matrix.xorHorizontally(BitMatrix.ALL_TRUE, ROW);

		for(int col = 0; col < matrix.getWidthInBits(); col++) {
			assertFalse(matrix.get(ROW, col));
		}
	}
}
