import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class SetTest {


	@Test
	public void shouldMatchOnlyOne() {
		BitMatrix matrix = new BitMatrix();
		matrix.setTrue(1, 1);

		assertTrue(matrix.get(1, 1));
		assertFalse(matrix.get(1, 0));
		assertFalse(matrix.get(0, 1));
		assertFalse(matrix.get(0, 0));
	}

	@Test
	public void shouldMatchOnlySameIndexes() {
		BitMatrix matrix = new BitMatrix();

		for(int i = 0; i < matrix.getWidthInBits(); i++) {
			matrix.setTrue(i, i);
		}

		for(int row = 0; row < matrix.getHeightInBits(); row++) {
			for(int column = 0; column < matrix.getWidthInBits(); column++) {
				if(column == row) {
					assertTrue(matrix.get(row, column));
				} else {
					assertFalse(matrix.get(row, column));
				}
			}
		}
	}

	@Test
	public void shouldBeAllFalse() {
		BitMatrix matrix = new BitMatrix();
		matrix.setTrue(1, 1);
		matrix.setTrue(1, 2);
		matrix.setTrue(1, 3);
		matrix.setTrue(3, 1);
		matrix.setTrue(2, 1);
		matrix.setFalse(1, 1);
		matrix.setFalse(1, 2);
		matrix.setFalse(1, 3);
		matrix.setFalse(3, 1);
		matrix.setFalse(2, 1);

		for(int row = 0; row < matrix.getHeightInBits(); row++) {
			for(int column = 0; column < matrix.getWidthInBits(); column++) {
				assertFalse(matrix.get(row, column));
			}
		}
	}
}
