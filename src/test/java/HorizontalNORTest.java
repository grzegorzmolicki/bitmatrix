import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(JUnit4.class)
public class HorizontalNORTest {

	@Test
	public void shouldMatchOnlyFirstCell() {
		BitMatrix matrix = new BitMatrix();

		matrix.notOrHorizontally(BitMatrix.LAST_FALSE, 0);

		assertTrue(matrix.get(0, 0));
		assertFalse(matrix.get(1, 0));
		assertFalse(matrix.get(1, 1));
		assertFalse(matrix.get(0, 1));
	}

	@Test
	public void shouldMatchOnlyFirstRow() {
		BitMatrix matrix = new BitMatrix();

		matrix.notOrHorizontally(0L, 1);

		for(int col = 0; col < matrix.getWidthInBits(); col++) {
			assertTrue(matrix.get(1, col));
		}

		for(int col = 0; col < matrix.getWidthInBits(); col++) {
			assertFalse(matrix.get(0, col));
			assertFalse(matrix.get(2, col));
		}
	}

	@Test
	public void shouldMatchOnlyEvenCols() {
		BitMatrix matrix = new BitMatrix();

		final int ROW = 4;

		matrix.notOrHorizontally(BitMatrix.ODD_TRUE, ROW);

		for(int col = 0; col < matrix.getWidthInBits(); col += 2) {
			assertTrue(matrix.get(ROW, col));
		}

		for(int col = 1; col < matrix.getWidthInBits(); col += 2) {
			assertFalse(matrix.get(ROW, col));
		}
	}
}
