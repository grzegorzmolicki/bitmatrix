import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(JUnit4.class)
public class VerticalANDTest {

	@Test
	public void shouldMatchOnlySingleBit() {
		BitMatrix matrix = new BitMatrix();
		matrix.setTrue(0, 0);

		matrix.andVertically(1L, 0);

		assertTrue(matrix.get(0, 0));
		assertFalse(matrix.get(1, 0));
		assertFalse(matrix.get(2, 0));
		assertFalse(matrix.get(3, 0));
		assertFalse(matrix.get(0, 1));
		assertFalse(matrix.get(1, 2));
		assertFalse(matrix.get(2, 3));

		matrix.andVertically(0L, 0);
		assertFalse(matrix.get(0, 0));
	}

	@Test
	public void shouldMatchOnlyFourthColumn() {
		BitMatrix matrix = new BitMatrix();
		matrix.orVertically(BitMatrix.ALL_TRUE, 4);
		matrix.andVertically(BitMatrix.ALL_TRUE, 4);

		for (int row = 0; row < matrix.getHeightInBits(); row++) {
			assertTrue(matrix.get(row, 4));
		}
	}

	@Test
	public void sholdMatchOnlyEvenRowsInFourthColumn() {
		final int COL = 4;
		BitMatrix matrix = new BitMatrix();
		matrix.orVertically(BitMatrix.EVEN_TRUE, COL);

		matrix.andVertically(BitMatrix.ALL_TRUE, COL);
		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, COL));
		}

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, COL));
		}
	}

	@Test
	public void shouldMatchOnlyOddRowsInFourthColumn() {
		final int COL = 4;
		BitMatrix matrix = new BitMatrix();
		matrix.orVertically(BitMatrix.ODD_TRUE, COL);

		matrix.andVertically(BitMatrix.ALL_TRUE, COL);
		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, COL));
		}

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, COL));
		}
	}

}
