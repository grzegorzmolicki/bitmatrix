import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class HorizontalORTest {

	@Test
	public void shoulMatchOnlyFirstRow() {
		BitMatrix matrix = new BitMatrix();
		matrix.orHorizontally(BitMatrix.ALL_TRUE, 0);

		for(int i = 0; i < matrix.getWidthInBits(); i++) {
			assertTrue(matrix.get(0, i));
		}

		for(int row = 1; row < matrix.getHeightInBits(); row++) {
			for(int col = 0; col < matrix.getWidthInBits(); col++) {
				assertFalse(matrix.get(row, col));
			}
		}
	}

	@Test
	public void shouldMatchOnlyEvenColumns() {
		BitMatrix matrix = new BitMatrix();
		matrix.orHorizontally(BitMatrix.EVEN_TRUE, 0);

		for(int i = 0; i < matrix.getWidthInBits(); i += 2) {
			assertTrue(matrix.get(0, i));
		}

		for(int i = 1; i < matrix.getWidthInBits(); i += 2) {
			assertFalse(matrix.get(0, i));
		}
	}

	@Test
	public void shouldMatchOnlyOddColumns() {
		BitMatrix matrix = new BitMatrix();
		matrix.orHorizontally(BitMatrix.ODD_TRUE, 0);

		for(int i = 1; i < matrix.getWidthInBits(); i += 2) {
			assertTrue(matrix.get(0, i));
		}

		for(int i = 0; i < matrix.getWidthInBits(); i += 2) {
			assertFalse(matrix.get(0, i));
		}
	}
}
