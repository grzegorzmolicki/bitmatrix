import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(JUnit4.class)
public class HorizontalANDTest {

	@Test
	public void shouldMatchOnlyFirstCell() {
		BitMatrix matrix = new BitMatrix();
		matrix.setTrue(0, 0);

		matrix.andHorizontally(BitMatrix.ALL_TRUE, 0);

		assertTrue(matrix.get(0, 0));
		assertFalse(matrix.get(1, 0));
		assertFalse(matrix.get(1, 1));
		assertFalse(matrix.get(0, 1));
	}

	@Test
	public void shouldMatchOnlyFourthRow() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();
		matrix.orHorizontally(BitMatrix.ALL_TRUE, ROW);

		matrix.andHorizontally(BitMatrix.ALL_TRUE, ROW);

		for(int col = 0; col < matrix.getWidthInBits(); col++) {
			assertTrue(matrix.get(ROW, col));
		}

		for(int col = 0; col < matrix.getWidthInBits(); col++) {
			assertFalse(matrix.get(ROW-1, col));
			assertFalse(matrix.get(ROW+1, col));
		}
	}

	@Test
	public void shouldMatchOnlyEvenColsInFourthRow() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();
		matrix.orHorizontally(BitMatrix.EVEN_TRUE, ROW);

		for(int col = 0; col < matrix.getWidthInBits(); col += 2) {
			assertTrue(matrix.get(ROW, col));
		}

		for(int col = 1; col < matrix.getWidthInBits(); col += 2) {
			assertFalse(matrix.get(ROW, col));
		}
	}

	@Test
	public void shouldMatchOnlyOddColsInFourthRow() {
		final int ROW = 4;
		BitMatrix matrix = new BitMatrix();
		matrix.orHorizontally(BitMatrix.ODD_TRUE, ROW);

		for(int col = 1; col < matrix.getWidthInBits(); col += 2) {
			assertTrue(matrix.get(ROW, col));
		}

		for(int col = 0; col < matrix.getWidthInBits(); col += 2) {
			assertFalse(matrix.get(ROW, col));
		}
	}

}
