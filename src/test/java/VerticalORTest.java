import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class VerticalORTest {

	@Test
	public void shouldHaveWholeFirstColumnTrue() {
		BitMatrix matrix = new BitMatrix();
		matrix.orVertically(BitMatrix.ALL_TRUE, 0);

		for(int i = 0; i < matrix.getHeightInBits(); i++) {
			assertTrue(matrix.get(i, 0));
		}

		for(int row = 0; row < matrix.getHeightInBits(); row++) {
			for(int col = 1; col < matrix.getWidthInBits(); col++) {
				assertFalse(matrix.get(row, col));
			}
		}
	}

	@Test
	public void shouldHaveEvenRowsInFirstColumnTrue() {
		BitMatrix matrix = new BitMatrix();
		matrix.orVertically(BitMatrix.EVEN_TRUE, 0);

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, 0));
		}

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, 0));
		}
	}

	@Test
	public void shouldHaveOddRowsInFirstColumnTrue() {
		BitMatrix matrix = new BitMatrix();
		matrix.orVertically(BitMatrix.ODD_TRUE, 0);

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, 0));
		}

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, 0));
		}
	}

}
