import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@RunWith(JUnit4.class)
public class VerticalNORTest {

	@Test
	public void shouldMatchOnlyFirstColumn() {
		BitMatrix matrix = new BitMatrix();

		matrix.notOrVertically(0L, 0);

		for(int row = 0; row < matrix.getHeightInBits(); row++) {
			assertTrue(matrix.get(row, 0));

			for(int col = 1; col < matrix.getWidthInBits(); col++) {
				assertFalse(matrix.get(row, col));
			}
		}
	}

	@Test
	public void shouldMatchOnlyEvenRowsFourthColumn() {
		BitMatrix matrix = new BitMatrix();
		final int COL = 4;

		matrix.notOrVertically(BitMatrix.ODD_TRUE, COL);

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, COL));
		}

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, COL));
		}
	}

	@Test
	public void shouldMatchOnlyOddRowsFourthColumn() {
		BitMatrix matrix = new BitMatrix();
		final int COL = 4;

		matrix.notOrVertically(BitMatrix.EVEN_TRUE, COL);

		for(int row = 1; row < matrix.getHeightInBits(); row += 2) {
			assertTrue(matrix.get(row, COL));
		}

		for(int row = 0; row < matrix.getHeightInBits(); row += 2) {
			assertFalse(matrix.get(row, COL));
		}
	}
}
