import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.stream.Collectors;

/*
    TODO
    - Shift bits in column/row by [param]
    - Shift bits with moving last bit to first position  aka rotations
    - SafeMode (if in safe mode then throw exceptions when different sizes)
        If not, then ensure capacity to given argument
    - Operations from boolean[]
    - More words than just 1 word width
 */
public class BitMatrix {
    // For now lets assume that we will only need 64 bits width
    private static final short INITIAL_WIDTH = 1;
    private static final short INITIAL_HEIGHT = 64;

    public static final long ALL_TRUE = 0b1111111111111111111111111111111111111111111111111111111111111111L;
    public static final long LAST_FALSE = 0b1111111111111111111111111111111111111111111111111111111111111110L;
    public static final long EVEN_TRUE = 0b0101010101010101010101010101010101010101010101010101010101010101L;
    public static final long ODD_TRUE = 0b1010101010101010101010101010101010101010101010101010101010101010L;

    private int width;
    private int height;

    // long[width][height] matrix for now, just to make development easier I'm using only 1 word width array
    private long[] matrix;

    public BitMatrix() {
        matrix = new long[INITIAL_HEIGHT];

        this.width = INITIAL_WIDTH;
        this.height = INITIAL_HEIGHT;
    }

    public BitMatrix(int width, int height) {
        matrix = new long[height];

        this.width = width;
        this.height = height;
    }

    public long getWidthInBits() {
        return this.width*Long.SIZE;
    }

    public long getHeightInBits() {
        return this.height;
    }

    public void setTrue(int col, int row) {
        matrix[row] |= (1L << col);
    }

    public void setFalse(int col, int row) {
        matrix[row] &= ~(1L << col);
    }

    public boolean get(int row, int col) {
        long test = 1L << col;
        return (matrix[row] & test) != 0;
    }

    public void orVertically(long bitSet, int col) {
        long test;
        for (int i = 0; i < 64; i++) {
            test = (bitSet & 1L) << col;
            matrix[i] |= test;
            bitSet = bitSet >> 1;
        }
    }

    public void orHorizontally(long bitSet, int row) {
        matrix[row] |= bitSet;
    }

    /**
     * Operation bitset & 1L gets youngest bit in the word
     * Then creates mask with all not significant bits setTrue as true
     * After that bits are rotated to the left by @col
     * Finally and operation is being performed
     * @param bitSet
     * @param col
     */
    public void andVertically(long bitSet, int col) {
        long test;
        for (int row = 0; row < 64; row++) {
            test = Long.rotateLeft((bitSet & 1L) | LAST_FALSE, col);
            matrix[row] &= test;
            bitSet = bitSet >> 1;
        }
    }

    public void andHorizontally(long bitSet, int row) {
        matrix[row] &= bitSet;
    }

    public void xorVertically(long bitSet, int col) {
        long test;
        for (int row = 0; row < 64; row++) {
            test = Long.rotateLeft(bitSet & 1L, col);
            matrix[row] ^= test;
            bitSet = bitSet >> 1;
        }
    }

    public void xorHorizontally(long bitSet, int row) {
        matrix[row] ^= bitSet;
    }

    public void notOrHorizontally(long bitSet, int row) {
        matrix[row] |= bitSet;
        matrix[row] = ~matrix[row];
    }

    public void notOrVertically(long bitset, int col) {
        long test;
        for(int i = 0; i < 64; ++i) {
            test = (bitset & 1L) << col;
            matrix[i] |= test;
            matrix[i] ^= 1L << col;
            bitset = bitset >> 1;
        }
    }

    public void notAndHorizontally(long bitSet, int row) {
        matrix[row] &= bitSet;
        matrix[row] = ~matrix[row];
    }

    public void notAndVertically(long bitSet, int col) {

    }

    public void shiftDown(int n, int col) {
        throw new NotImplementedException();
    }

    public void shiftUp(int n, int col) {
        throw new NotImplementedException();
    }

    public void shiftRight(int n, int row) {
        matrix[row] = matrix[row] >> n;

    }

    public void shiftRightLeadingTrue(int n, int row) {
        matrix[row] = matrix[row] >>> n;
    }

    public void shiftLeft(int n, int row) {
        matrix[row] = matrix[row] << n;
    }

    /*
        TODO
            Those below are demanding, it'll be fun

     */

    public void shiftDownWithFollowingTrue(int n, int col) {
        throw new NotImplementedException();
    }

    public void shiftUpWithFollowingTrue(int n, int col) {
        throw new NotImplementedException();
    }

    public void shiftRightWithFollowingTrue(int n, int row) {
        throw new NotImplementedException();
    }

    public void shiftLeftWithFollowingTrue(int n, int row) {
        throw new NotImplementedException();
    }

    @Override
    public String toString() {
        return Arrays.stream(matrix)
                     .mapToObj(Long::toBinaryString)
                     .collect(Collectors.joining("\n"));
    }
}
